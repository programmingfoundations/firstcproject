//Listado de bibliotecas utilizadas
#include<stdio.h>
#include <wchar.h>
#include <locale.h>
//Esta Biblioteca es útil en este programa para poder escribir la letra ñ y acentos
#include<stdio.h>
#include<stdlib.h>
// #include <conio.h>
//Tamaño del arreglo bidimensional utilizado
//También nos sirve para cambiar el tamaño de la introducción de datos de manera ágil
#define ULTIMA_HORA 24
#define ULTIMO_DIA 7
//declaración de encabezados de funciones
void introduce_datos(float temp[][ULTIMO_DIA],int dia,int hora);
void visualiza_datos(float temp[][ULTIMO_DIA],int dia,int hora);
void encontrar_maximo_semanal(float temp[][ULTIMO_DIA],int hora,int dia);
void encontrar_minimo_semanal(float temp[][ULTIMO_DIA],int hora,int dia);
void encontrar_maximo_diario(float temp[][ULTIMO_DIA],int hora,int dia);
void encontrar_minimo_diario(float temp[] [ULTIMO_DIA], int hora, int dia);
void promedio_semanal(float temp[][ULTIMO_DIA], int hora,int dia);
void promedio_diario(float temp[][ULTIMO_DIA], int hora,int dia);
void promedio_diario_superior(float temp[][ULTIMO_DIA], int hora,int dia);
void limpia_datos(float temp[][ULTIMO_DIA],int hora,int dia);
void dia_de_la_semana(int dia);
//comienzo del programa principal:
int main() {
    //Declaración de variables utilizadas
    int dia;
    int hora;
    int op;
    //Arreglo bidimensional para almacenar las temperaturas registradas
    float temp[ULTIMA_HORA][ULTIMO_DIA];
    //Para poder escribir la letra ñ y los acentos junto con la biblioteca locale.h
    setlocale(LC_ALL,"");

    do {
        // mostrar menu
        printf("\n");
        printf("REGISTRO E INFORME DE TEMPERATURA SEMANAL CDMX\n\n");
        printf("   1. Introducción de TEMPERATURA semanal por DIA y HORA\n");
        printf("   2. Visualizar TEMPERATURAS INGRESADAS\n");
        printf("   3. Temperatura MAXIMA de la SEMANA\n");
        printf("   4. Temperatura MINIMA de la SEMANA\n");
        printf("   5. Temperatura MAXIMA de cada DIA\n");
        printf("   6. Temperatura MINIMA de cada DIA\n");
        printf("   7. Temperatura MEDIA de la SEMANA\n");
        printf("   8. Temperatura MEDIA de cada DIA\n");
        printf("   9. DIAS en los que la temperatura MEDIA es SUPERIOR a:\n");
        printf("   10. LIMPIAR datos de REGISTRO de TEMPERATURA\n");
        printf("   11. Salir\n");
        // ingresar una opcion
        printf("\nElija una opción (1-11): \t");
        scanf("%i",&op);
        // procesar opción
        switch (op) {
            case 1:
                //función para introducir temperaturas
                introduce_datos(temp,dia,hora);
                break;
            case 2:
                //función para visualizar las temperaturas
                visualiza_datos(temp,dia,hora);
                break;
            case 3:
                //función para determinar la másxima temperatura semanal
                encontrar_maximo_semanal(temp,hora,dia);
                break;
            case 4:
                //función para determinar la minima temperatura semanal
                encontrar_minimo_semanal(temp,hora,dia);
                break;
            case 5:
                //función para determinar la temperatura máxima diaria
                encontrar_maximo_diario(temp,hora,dia);
                break;
            case 6:
                //función para determinar la temperatura mínima diaria
                encontrar_minimo_diario(temp,hora,dia);
                break;
            case 7:
                //función para determinar la temperatura promedio semanal
                promedio_semanal(temp,hora,dia);
                break;
            case 8:
                //función para determinar la temperatura promedio diaria
                promedio_diario(temp,hora,dia);
                break;
            case 9:
                //función para determinar las temperaturas superiores a:
                promedio_diario_superior(temp,hora,dia);
                break;
            case 10:
                //función para limpiar el la tabla de temperaturas
                limpia_datos(temp,dia,hora);
                break;
            case 11:
                printf("¡Gracias, vuelva pronto!\n");
                break;
            default:
                printf("Opción no válida\n");
        }
        //printf("Presione enter para continuar\n");
        getchar(); /* a diferencia del pseudocódigo, espera un Enter, no cualquier tecla */

    } while (op!=11);
    return 0;
}



void introduce_datos(float temp[][ULTIMO_DIA],int dia,int hora)
{

    printf("Introduzca los datos de temperatura...\n\n");
    for (dia=1;dia<=ULTIMO_DIA;dia+=1) {
        dia_de_la_semana(dia);
        for (hora=1;hora<=ULTIMA_HORA;hora+=1) {
            printf("%d:00 Horas:\t",hora);
            scanf("%f",&temp[hora-1][dia-1]);
        }
    }

    printf("...Datos completados!\n\n");
    system("pause");/*hará una pausa antes de terminar la ejecución*/
    system("cls"); /*limpiará la pantalla*/


}

void visualiza_datos(float temp[][ULTIMO_DIA],int dia,int hora)
{

    printf("Los datos introducidos son los siguientes:\n");
    for (dia=1;dia<=ULTIMO_DIA;dia+=1) {
        dia_de_la_semana(dia);
        //printf("\n\nTemperaturas del dia %d:\n\n",dia);
        for (hora=1;hora<=ULTIMA_HORA;hora+=1) {

            if (hora % 25 ==0)
                printf("\n");
            //printf("[%.1f]\t",temp[hora-1][dia-1]);
            printf("[%.1f°C]\t",temp[hora-1][dia-1]);
        }
    }

    printf("\n\n");
    system("pause");/*hará una pausa antes de terminar la ejecución*/
    system("cls"); /*limpiará la pantalla*/

}


void encontrar_maximo_semanal(float temp[][ULTIMO_DIA],int hora,int dia)
{
    float maximo = 0;

    for (dia = 1; dia <= ULTIMO_DIA; dia++)
        for (hora = 1; hora <= ULTIMA_HORA; hora++)
            if (temp[hora-1] [dia-1] > maximo)
                maximo = temp[hora-1] [dia-1];

    printf( " \n\n La Temperatura MAXIMA de la SEMANA es: %.1f °C \n\n\n ", maximo);


    system("pause");/*hará una pausa antes de terminar la ejecución*/
    system("cls"); /*limpiará la pantalla*/


}


void encontrar_minimo_semanal(float temp[] [ULTIMO_DIA], int hora, int dia)
{
    float minimo = 100.1;
    for (dia = 1; dia <= ULTIMO_DIA; dia++)
        for (hora = 1; hora <= ULTIMA_HORA; hora++)
            if (temp[hora-1] [dia-1] < minimo)
                minimo = temp[hora-1][dia-1];

    printf( " \n\n La Temperatura MINIMA de la SEMANA es: %.1f °C \n\n\n ", minimo);


    system("pause");/*hará una pausa antes de terminar la ejecución*/
    system("cls"); /*limpiará la pantalla*/

}

void encontrar_maximo_diario(float temp[][ULTIMO_DIA],int hora,int dia)
{
    float maximo;

    for (dia = 1; dia <= ULTIMO_DIA; dia++){

        maximo=0;

        for (hora = 1; hora <= ULTIMA_HORA; hora++)
            if (temp[hora-1] [dia-1] > maximo)
                maximo = temp[hora-1] [dia-1];

        printf( " \n\n La Temperatura MAXIMA del DIA %d es: %.1f °C \n ",dia, maximo);
    }

    printf( " \n\n" );
    system("pause");/*hará una pausa antes de terminar la ejecución*/
    system("cls"); /*limpiará la pantalla*/

}


void encontrar_minimo_diario(float temp[] [ULTIMO_DIA], int hora, int dia)
{
    float minimo;

    for (dia = 1; dia <= ULTIMO_DIA; dia++){

        minimo=100;

        for (hora = 1; hora <= ULTIMA_HORA; hora++)
            if (temp[hora-1] [dia-1] < minimo)
                minimo = temp[hora-1][dia-1];

        printf( " \n\n La Temperatura MINIMA del DIA %d es: %.1f °C \n ",dia, minimo);
    }
    printf( " \n\n" );
    system("pause");/*hará una pausa antes de terminar la ejecución*/
    system("cls"); /*limpiará la pantalla*/
}


void promedio_semanal(float temp[][ULTIMO_DIA], int hora,int dia)
{
    int elementos=ULTIMO_DIA*ULTIMA_HORA;
    float total= 0;
    float promedio;

    for (dia = 1; dia <= ULTIMO_DIA; dia++)
        for (hora = 1; hora <= ULTIMA_HORA; hora++)
            total = total+temp[hora-1][dia-1];

    promedio= total/elementos;
    printf( " \n\n La Temperatura MEDIA de la SEMANA es: %.1f °C \n ", promedio);

    printf( " \n\n");
    system("pause");/*hará una pausa antes de terminar la ejecución*/
    system("cls"); /*limpiará la pantalla*/

}


void promedio_diario(float temp[][ULTIMO_DIA], int hora,int dia)
{
    int elementos=ULTIMA_HORA;
    float total;
    float promedio;

    for (dia = 1; dia <= ULTIMO_DIA; dia++){

        total=0;

        for (hora = 1; hora <= ULTIMA_HORA; hora++)
            total = total+temp[hora-1][dia-1];

        promedio= total/elementos;
        printf( " \n\n La Temperatura MEDIA del DIA %d es: %.1f °C \n ",dia, promedio);

    }

    printf( " \n\n");
    system("pause");/*hará una pausa antes de terminar la ejecución*/
    system("cls"); /*limpiará la pantalla*/
}


void promedio_diario_superior(float temp[][ULTIMO_DIA], int hora,int dia)
{
    int elementos=ULTIMA_HORA;
    float total;
    float promedio;
    float superior;
    int cuenta_dias=0;

    printf("  \n\n Ingrese la TEMPERATURA MEDIA que está BUSCANDO: \t");
    scanf("%f",&superior);

    for (dia = 1; dia <= ULTIMO_DIA; dia++){

        total=0;

        for (hora = 1; hora <= ULTIMA_HORA; hora++)
            total = total+temp[hora-1][dia-1];

        promedio= total/elementos;
        if (promedio > superior){
            cuenta_dias=cuenta_dias+1;

            printf( " \n\n La MEDIA del DIA %d es de : %.1f °C , supera la MEDIA buscada!\n ",dia, promedio);
        }
    }
    printf( " \n\n En total hubo: %d día(s) en los que se SUPERÓ la MEDIA buscada! \n ",cuenta_dias);
    printf( " \n\n");
    system("pause");/*hará una pausa antes de terminar la ejecución*/
    system("cls"); /*limpiará la pantalla*/
}

void limpia_datos(float temp[][ULTIMO_DIA],int dia,int hora)
{
    for (dia=1;dia<=ULTIMO_DIA;dia+=1) {
        for (hora=1;hora<=ULTIMA_HORA;hora+=1) {
            temp[hora-1][dia-1] = 0;
        }
    }
    printf("\n\nEl registro de temperatura ha sido borrado\n\n");
    printf("\n\nPuede verificar con la opción 2\n\n");
    system("pause");/*hará una pausa antes de terminar la ejecución*/
    system("cls"); /*limpiará la pantalla*/

}

//función que determina de acuerdo al día de la semana el nombre del día
void dia_de_la_semana(int dia)
{
    switch ( dia )
    {
        case 1 : printf( "\n\nTemperaturas del día 1->LUNES:\n\n" );
            break;
        case 2 : printf( "\n\nTemperaturas del día 2->MARTES:\n\n" );
            break;
        case 3 : printf( "\n\nTemperaturas del día 3->MIÉRCOLES:\n\n" );
            break;
        case 4 : printf( "\n\nTemperaturas del día 4->JUEVES:\n\n" );
            break;
        case 5 : printf( "\n\nTemperaturas del día 5->VIERNES:\n\n" );
            break;
        case 6 : printf( "\n\nTemperaturas del día 6->SÁBADO:\n\n" );
            break;
        case 7 : printf( "\n\nTemperaturas del día 7->DOMINGO:\n\n" );
            break;
        default : printf( "ERROR: Día incorrecto.");
    }
}
 